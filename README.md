# Zink XDC 2019 slide-deck

Here's the slides for [my talk about Zink][talk] for [XDC 2019][xdc19].

The source is Markdown, and requires [Marp][marp] to build.

[talk]: https://xdc2019.x.org/event/5/contributions/329/
[xdc19]: https://xdc2019.x.org/
[marp]: https://github.com/marp-team/marp
