.PHONY: all clean

all: slide-deck.html slide-deck.pdf

clean:
	$(RM) slide-deck.html slide-deck.pdf

slide-deck.html: slide-deck.md assets/
	marp slide-deck.md

slide-deck.pdf: slide-deck.md assets/
	marp --pdf --allow-local-files slide-deck.md
